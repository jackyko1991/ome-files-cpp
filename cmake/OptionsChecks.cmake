# #%L
# OME C++ libraries (cmake build infrastructure)
# %%
# Copyright © 2018-2019 Quantitative Imaging Systems, LLC
# %%
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# The views and conclusions contained in the software and documentation are
# those of the authors and should not be interpreted as representing official
# policies, either expressed or implied, of any organization.
# #L%

if (POLICY CMP0067)
  cmake_policy(SET CMP0067 NEW)
endif()
if (POLICY CMP0075)
  cmake_policy(SET CMP0075 NEW)
endif()

include(CheckCXXSourceCompiles)

set(options "boost;qt6;qt5" CACHE STRING "Use options parser from 'boost', 'qt6' or 'qt5'.  Multiple options will be used as fallbacks")

set(OME_OPTIONS NOTFOUND)

message(STATUS "Checking options implementations: ${options}")

foreach(opt ${options})
  if (opt STREQUAL "boost")
    set(Boost_USE_STATIC_LIBS OFF)
    set(Boost_USE_MULTITHREADED ON)
    set(Boost_USE_STATIC_LIBS OFF)
    find_package(Boost 1.53 COMPONENTS program_options)
    if (TARGET Boost::program_options)
      set(OME_HAVE_BOOST_OPTIONS TRUE)
      set(OME_OPTIONS TRUE)
      set(OPTIONS_LIBS Boost::program_options)
      message(STATUS "Using options: boost")
      break()
    endif()
  endif()
  if (opt STREQUAL "qt6")
    find_package(Qt6Core)
    find_package(Qt6Core5Compat)
    if (TARGET Qt6::Core AND TARGET Qt6::Core5Compat)
      set(OME_HAVE_QT6_OPTIONS TRUE)
      set(OME_HAVE_QT5_OPTIONS TRUE)
      set(OME_OPTIONS TRUE)
      set(OPTIONS_DEPS Qt6Core Qt6Core5Compat)
      set(OPTIONS_LIBS Qt6::Core Qt6::Core5Compat)
      message(STATUS "Using options: qt6")
      break()
    endif()
  endif()
  if (opt STREQUAL "qt5")
    find_package(Qt5Core)
    if (TARGET Qt5::Core)
      set(OME_HAVE_QT5_OPTIONS TRUE)
      set(OME_OPTIONS TRUE)
      set(OPTIONS_DEPS Qt5Core)
      set(OPTIONS_LIBS Qt5::Core)
      message(STATUS "Using options: qt5")
    endif()
  endif()
  if(OME_OPTIONS)
    break()
  endif()
endforeach()

if(NOT OME_OPTIONS)
  message(FATAL_ERROR "No options implementation found")
endif()

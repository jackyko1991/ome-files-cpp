/*
 * #%L
 * OME-FILES C++ library for image IO.
 * Copyright © 2006 - 2015 Open Microscopy Environment:
 *   - Massachusetts Institute of Technology
 *   - National Institutes of Health
 *   - University of Dundee
 *   - Board of Regents of the University of Wisconsin-Madison
 *   - Glencoe Software, Inc.
 * Copyright © 2018 Quantitative Imaging Systems, LLC
 * Copyright © 2019 Codelibre Consulting Limited
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of any organization.
 * #L%
 */

#include <cmath>
#include <filesystem>
#include <fstream>

#include <fmt/format.h>
#include <spdlog/spdlog.h>

#include <ome/files/FormatTools.h>
#include <ome/files/MetadataTools.h>
#include <ome/files/PixelBuffer.h>
#include <ome/files/PixelProperties.h>
#include <ome/files/VariantPixelBuffer.h>
#include <ome/files/detail/FormatReader.h>

#include <ome/xml/meta/DummyMetadata.h>
 #include <ome/xml/meta/MetadataStore.h>
#include <ome/xml/meta/OMEXMLMetadata.h>

using std::filesystem::path;
using ome::xml::meta::DummyMetadata;
using ome::files::CoreMetadata;

namespace ome
{
  namespace files
  {
    namespace detail
    {

      namespace
      {
        // Default thumbnail width and height.
        const dimension_size_type THUMBNAIL_DIMENSION = 128;
      }

      FormatReader::FormatReader(const ReaderProperties& readerProperties):
        readerProperties(readerProperties),
        currentId(std::nullopt),
        in(),
        metadata(),
        series(0),
        resolution(0),
        plane(0),
        core(),
        suffixNecessary(true),
        suffixSufficient(true),
        companionFiles(false),
        datasetDescription("Single file"),
        normalizeData(false),
        saveOriginalMetadata(false),
        indexedAsRGB(false),
        group(true),
        domains(),
        metadataStore(std::make_shared<DummyMetadata>()),
        metadataOptions()
      {
        assertId(currentId, false);
      }

      FormatReader::~FormatReader()
      {
      }

      const std::string&
      FormatReader::getFormat() const
      {
        return readerProperties.name;
      }

      const std::string&
      FormatReader::getFormatDescription() const
      {
        return readerProperties.description;
      }

      const std::vector<std::filesystem::path>&
      FormatReader::getSuffixes() const
      {
        return readerProperties.suffixes;
      }

      const std::vector<std::filesystem::path>&
      FormatReader::getCompressionSuffixes() const
      {
        return readerProperties.compression_suffixes;
      }

      const std::set<MetadataOptions::MetadataLevel>&
      FormatReader::getSupportedMetadataLevels()
      {
        return readerProperties.metadata_levels;
      }

      void
      FormatReader::initFile(const std::filesystem::path& id)
      {
        if (currentId)
          {
            for (const auto& file : getUsedFiles())
              if (id == file) return;
          }

        series = resolution = plane = 0;
        close();
        currentId = id;
        metadata.clear();

        core.clear();
        core.resize(1);
        core[0].emplace_back(std::make_unique<CoreMetadata>());


        // reinitialize the MetadataStore
        // NB: critical for metadata conversion to work properly!
        getMetadataStore()->createRoot();
      }

      bool
      FormatReader::isUsedFile(const std::filesystem::path& file)
      {
        bool used = false;

        try
          {
            path thisfile = std::filesystem::canonical(path(file));

            /// @todo: Use a set rather than a list?
            const std::vector<path>& s = getUsedFiles();
            for (const auto& file : getUsedFiles())
              {
                try
                  {
                    path usedfile = std::filesystem::canonical(file);
                    if (thisfile == usedfile)
                      {
                        used = true;
                        break;
                      }
                  }
                catch (const std::filesystem::filesystem_error&)
                  {
                  }
              }
          }
        catch (const std::filesystem::filesystem_error&)
          {
          }

        return used;
      }

      void
      FormatReader::setMetadataOptions(const MetadataOptions& options)
      {
        this->metadataOptions = options;
      }

      const MetadataOptions&
      FormatReader::getMetadataOptions() const
      {
        return metadataOptions;
      }

      MetadataOptions&
      FormatReader::getMetadataOptions()
      {
        return metadataOptions;
      }

      bool
      FormatReader::isThisType(const std::filesystem::path& name,
                               bool                         open) const
      {
        // if file extension ID is insufficient and we can't open the file, give up
        if (!suffixSufficient && !open)
          return false;

        path cname(std::filesystem::canonical(name));

        if (!std::filesystem::exists(cname))
          return false;

        if (suffixNecessary || suffixSufficient) {
          // it's worth checking the file extension
          bool suffixMatch = checkSuffix(cname,
                                         readerProperties.suffixes,
                                         readerProperties.compression_suffixes);

          // if suffix match is required but it doesn't match, failure
          if (suffixNecessary && !suffixMatch)
            return false;

          // if suffix matches and that's all we need, green light it
          if (suffixMatch && suffixSufficient)
            return true;
        }

        // suffix matching was inconclusive; we need to analyze the file contents
        if (!open)
          return false; // not allowed to open any files

        return isFilenameThisTypeImpl(cname);
      }

      bool
      FormatReader::isThisType(std::istream& stream) const
      {
        return isStreamThisTypeImpl(stream);
      }

      bool
      FormatReader::isFilenameThisTypeImpl(const std::filesystem::path& /* name */) const
      {
        return false;
      }

      bool
      FormatReader::isStreamThisTypeImpl(std::istream& /* stream */) const
      {
        return false;
      }

      dimension_size_type
      FormatReader::getImageCount() const
      {
        assertId(currentId, true);
        return getCoreMetadata(getSeries(), getResolution()).imageCount;
      }

      bool
      FormatReader::isRGB(dimension_size_type channel) const
      {
        assertId(currentId, true);
        return getRGBChannelCount(channel) > 1U;
      }

      dimension_size_type
      FormatReader::getSizeX() const
      {
        assertId(currentId, true);
        return getCoreMetadata(getSeries(), getResolution()).sizeX;
      }

      dimension_size_type
      FormatReader::getSizeY() const
      {
        assertId(currentId, true);
        return getCoreMetadata(getSeries(), getResolution()).sizeY;
      }

      dimension_size_type
      FormatReader::getSizeZ() const
      {
        assertId(currentId, true);
        return getCoreMetadata(getSeries(), getResolution()).sizeZ;
      }

      dimension_size_type
      FormatReader::getSizeT() const
      {
        assertId(currentId, true);
        return getCoreMetadata(getSeries(), getResolution()).sizeT;
      }

      dimension_size_type
      FormatReader::getSizeC() const
      {
        assertId(currentId, true);

        const std::vector<dimension_size_type>& c(getCoreMetadata(getSeries(), getResolution()).sizeC);

        return std::accumulate(c.begin(), c.end(), dimension_size_type(0));
      }

      ome::xml::model::enums::PixelType
      FormatReader::getPixelType() const
      {
        assertId(currentId, true);
        return getCoreMetadata(getSeries(), getResolution()).pixelType;
      }

      pixel_size_type
      FormatReader::getBitsPerPixel() const
      {
        assertId(currentId, true);
        if (getCoreMetadata(getSeries(), getResolution()).bitsPerPixel == 0) {
          return bitsPerPixel(getPixelType());
          /**
           * @todo: Move this logic into a CoreMetadata accessor. Why
           * are we modifying coremetadata during an explicitly
           * nonmodifying operation??
           */
          //   getCoreMetadata(getSeries(), getResolution()).bitsPerPixel =
          //   FormatTools.getBytesPerPixel(getPixelType()) * 8;
        }
        return getCoreMetadata(getSeries(), getResolution()).bitsPerPixel;
      }

      dimension_size_type
      FormatReader::getEffectiveSizeC() const
      {
        return getCoreMetadata(getSeries(), getResolution()).sizeC.size();
      }

      dimension_size_type
      FormatReader::getRGBChannelCount(dimension_size_type channel) const
      {
        assertId(currentId, true);
        return getCoreMetadata(getSeries(), getResolution()).sizeC.at(channel);
      }

      bool
      FormatReader::isIndexed() const
      {
        assertId(currentId, true);
        return getCoreMetadata(getSeries(), getResolution()).indexed;
      }

      bool
      FormatReader::isFalseColor() const
      {
        assertId(currentId, true);
        return getCoreMetadata(getSeries(), getResolution()).falseColor;
      }

      void
      FormatReader::getLookupTable(dimension_size_type /* plane */,
                                   VariantPixelBuffer& /* buf */) const
      {
        assertId(currentId, true);

        throw std::runtime_error("Reader does not implement lookup tables");
      }

      Modulo&
      FormatReader::getModuloZ()
      {
        return getCoreMetadata(getSeries(), getResolution()).moduloZ;
      }

      const Modulo&
      FormatReader::getModuloZ() const
      {
        return getCoreMetadata(getSeries(), getResolution()).moduloZ;
      }

      Modulo&
      FormatReader::getModuloT()
      {
        return getCoreMetadata(getSeries(), getResolution()).moduloT;
      }

      const Modulo&
      FormatReader::getModuloT() const
      {
        return getCoreMetadata(getSeries(), getResolution()).moduloT;
      }

      Modulo&
      FormatReader::getModuloC()
      {
        return getCoreMetadata(getSeries(), getResolution()).moduloC;
      }

      const Modulo&
      FormatReader::getModuloC() const
      {
        return getCoreMetadata(getSeries(), getResolution()).moduloC;
      }

      std::array<dimension_size_type, 2>
      FormatReader::getThumbSize() const
      {
        assertId(currentId, true);

        std::array<dimension_size_type, 2> ret;
        ret[0] = getCoreMetadata(getSeries(), getResolution()).thumbSizeX;
        ret[1] = getCoreMetadata(getSeries(), getResolution()).thumbSizeY;

        if (ret[0] == 0 || ret[1] == 0)
          {
            dimension_size_type sx = getSizeX();
            dimension_size_type sy = getSizeY();

            if (sx < THUMBNAIL_DIMENSION && sy < THUMBNAIL_DIMENSION)
              {
                ret[0] = sx;
                ret[1] = sy;
              }
            else
              {
                // Assume sx and sy are equal initially.
                ret[0] = ret[1] = THUMBNAIL_DIMENSION;
                if (sx > sy)
                  {
                    if (sx > 0)
                      ret[1] = (sy * THUMBNAIL_DIMENSION) / sx;
                  }
                else if (sx < sy)
                  {
                    if (sy > 0)
                      ret[0] = (sx * THUMBNAIL_DIMENSION) / sy;
                  }
              }
          }

        if (ret[0] == 0)
          ret[0] = 1;
        if (ret[1] == 0)
          ret[1] = 1;

        return ret;
      }

      dimension_size_type
      FormatReader::getThumbSizeX() const
      {
        return getThumbSize()[0];
      }

      dimension_size_type
      FormatReader::getThumbSizeY() const
      {
        return getThumbSize()[1];
      }

      bool
      FormatReader::isLittleEndian() const
      {
        assertId(currentId, true);
        return getCoreMetadata(getSeries(), getResolution()).littleEndian;
      }

      const std::string&
      FormatReader::getDimensionOrder() const
      {
        assertId(currentId, true);
        return getCoreMetadata(getSeries(), getResolution()).dimensionOrder;
      }

      bool
      FormatReader::isOrderCertain() const
      {
        assertId(currentId, true);
        return getCoreMetadata(getSeries(), getResolution()).orderCertain;
      }

      bool
      FormatReader::isThumbnailSeries() const
      {
        assertId(currentId, true);
        return getCoreMetadata(getSeries(), getResolution()).thumbnail;
      }

      bool
      FormatReader::isInterleaved() const
      {
        return isInterleaved(0);
      }

      bool
      FormatReader::isInterleaved(dimension_size_type /* channel */) const
      {
        assertId(currentId, true);
        return getCoreMetadata(getSeries(), getResolution()).interleaved;
      }

      void
      FormatReader::openBytes(dimension_size_type plane,
                              VariantPixelBuffer& buf) const
      {
        openBytes(plane, buf, 0, 0, getSizeX(), getSizeY());
      }

      void
      FormatReader::openBytes(dimension_size_type plane,
                              VariantPixelBuffer& buf,
                              dimension_size_type x,
                              dimension_size_type y,
                              dimension_size_type w,
                              dimension_size_type h) const
      {
        setPlane(plane);
        openBytesImpl(plane, buf, x, y, w, h);
      }

      void
      FormatReader::openThumbBytes(dimension_size_type /* plane */,
                                   VariantPixelBuffer& /* buf */) const
      {
        assertId(currentId, true);
        /**
         * @todo Implement openThumbBytes.  This requires implementing
         * image rescaling/resampling.
         */
        throw std::runtime_error("Thumbnail rescaling is not currently implemented");
      }

      void
      FormatReader::close(bool fileOnly)
      {
        if (in)
          in = std::shared_ptr<std::istream>(); // set to null.
        if (!fileOnly)
          {
            currentId = std::nullopt;
            series = resolution = plane = 0;
            core.clear();
          }
      }

      dimension_size_type
      FormatReader::getSeriesCount() const
      {
        assertId(currentId, true);
        return core.size();
      }

      void
      FormatReader::setSeries(dimension_size_type series) const
      {
        if (series >= getSeriesCount())
          {
            std::string fs = fmt::format("Invalid series: {}", series);
            throw std::logic_error(fs);
          }
        this->series = series;
        this->resolution = 0;
        this->plane = 0;
      }

      dimension_size_type
      FormatReader::getSeries() const
      {
        return series;
      }

      void
      FormatReader::setPlane(dimension_size_type plane) const
      {
        assertId(currentId, true);

        if (plane >= getImageCount())
          {
            std::string fs = fmt::format("Invalid plane: {}", plane);
            throw std::logic_error(fs);
          }

        this->plane = plane;
      }

      dimension_size_type
      FormatReader::getPlane() const
      {
        assertId(currentId, true);

        return plane;
      }

      void
      FormatReader::setGroupFiles(bool group)
      {
        assertId(currentId, false);
        this->group = group;
      }

      bool
      FormatReader::isGroupFiles() const
      {
        return group;
      }

      FormatReader::FileGroupOption
      FormatReader::fileGroupOption(const std::string& /* id */)
      {
        return CANNOT_GROUP;
      }

      bool
      FormatReader::isMetadataComplete() const
      {
        assertId(currentId, true);
        return getCoreMetadata(getSeries(), getResolution()).metadataComplete;
      }

      void
      FormatReader::setNormalized(bool normalize)
      {
        assertId(currentId, false);
        normalizeData = normalize;
      }

      bool
      FormatReader::isNormalized() const
      {
        return normalizeData;
      }

      void
      FormatReader::setOriginalMetadataPopulated(bool populate)
      {
        assertId(currentId, false);
        saveOriginalMetadata = populate;
      }

      bool
      FormatReader::isOriginalMetadataPopulated() const
      {
        return saveOriginalMetadata;
      }

      const std::vector<std::filesystem::path>
      FormatReader::getUsedFiles(bool noPixels) const
      {
        SaveSeries sentry(*this);
        std::set<path> files;
        for (dimension_size_type i = 0; i < getSeriesCount(); ++i)
          {
            setSeries(i);
            for (const auto& file : getSeriesUsedFiles(noPixels))
              files.insert(file);
          }
        return std::vector<path>(files.begin(), files.end());
      }

      const std::vector<std::filesystem::path>
      FormatReader::getSeriesUsedFiles(bool noPixels) const
      {
        std::vector<path> ret;
        if (!noPixels && currentId)
          ret.push_back(*currentId);
        return ret;
      }

      std::vector<FileInfo>
      FormatReader::getAdvancedUsedFiles(bool noPixels) const
      {
        std::vector<path> files = getUsedFiles(noPixels);
        std::vector<FileInfo> infos(files.size());

        for (const auto& file : files)
          {
            FileInfo info;
            info.filename = file;
            info.reader = getFormat();
            info.usedToInitialize = false;

            const std::optional<path> currentfile = getCurrentFile();
            if (currentfile)
              {
                path current = std::filesystem::canonical(*currentfile);
                path thisfile = std::filesystem::canonical(file);

                info.usedToInitialize = (thisfile == current);
              }

            infos.push_back(info);
          }
        return infos;
      }

      std::vector<FileInfo>
      FormatReader::getAdvancedSeriesUsedFiles(bool noPixels) const
      {
        std::vector<path> files = getSeriesUsedFiles(noPixels);
        std::vector<FileInfo> infos(files.size());

        for (const auto& file : files)
          {
            FileInfo info;
            info.filename = file;
            info.reader = getFormat();
            info.usedToInitialize = false;

            const std::optional<path> currentid = getCurrentFile();
            if (currentid)
              {
                path current = std::filesystem::canonical(*currentid);
                path thisfile = std::filesystem::canonical(file);

                info.usedToInitialize = (thisfile == current);
              }

            infos.push_back(info);
          }
        return infos;
      }

      const std::optional<std::filesystem::path>&
      FormatReader::getCurrentFile() const
      {
        return currentId;
      }

      dimension_size_type
      FormatReader::getIndex(dimension_size_type z,
                             dimension_size_type c,
                             dimension_size_type t) const
      {
        assertId(currentId, true);
        return ome::files::getIndex(getDimensionOrder(),
                                         getSizeZ(),
                                         getEffectiveSizeC(),
                                         getSizeT(),
                                         getImageCount(),
                                         z, c, t);
      }

      dimension_size_type
      FormatReader::getIndex(dimension_size_type z,
                             dimension_size_type c,
                             dimension_size_type t,
                             dimension_size_type moduloZ,
                             dimension_size_type moduloC,
                             dimension_size_type moduloT) const
      {
        assertId(currentId, true);
        return ome::files::getIndex(getDimensionOrder(),
                                         getSizeZ(),
                                         getEffectiveSizeC(),
                                         getSizeT(),
                                         getModuloZ().size(),
                                         getModuloC().size(),
                                         getModuloT().size(),
                                         getImageCount(),
                                         z, c, t,
                                         moduloZ, moduloC, moduloT);
      }

      std::array<dimension_size_type, 3>
      FormatReader::getZCTCoords(dimension_size_type index) const
      {
        assertId(currentId, true);
        return ome::files::getZCTCoords(getDimensionOrder(),
                                             getSizeZ(),
                                             getEffectiveSizeC(),
                                             getSizeT(),
                                             getImageCount(),
                                             index);
      }

      std::array<dimension_size_type, 6>
      FormatReader::getZCTModuloCoords(dimension_size_type index) const
      {
        assertId(currentId, true);
        return ome::files::getZCTCoords(getDimensionOrder(),
                                             getSizeZ(),
                                             getEffectiveSizeC(),
                                             getSizeT(),
                                             getModuloZ().size(),
                                             getModuloC().size(),
                                             getModuloT().size(),
                                             getImageCount(),
                                             index);
      }

      const MetadataMap::value_type&
      FormatReader::getMetadataValue(const std::string& field) const
      {
        return metadata.get<MetadataMap::value_type>(field);
      }

      const MetadataMap::value_type&
      FormatReader::getSeriesMetadataValue(const MetadataMap::key_type& field) const
      {
        assertId(currentId, true);
        return getSeriesMetadata().get<MetadataMap::value_type>(field);
      }

      const MetadataMap&
      FormatReader::getGlobalMetadata() const
      {
        return metadata;
      }

      const MetadataMap&
      FormatReader::getSeriesMetadata() const
      {
        assertId(currentId, true);
        return getCoreMetadata(getSeries(), getResolution()).seriesMetadata;
      }

      const CoreMetadataList&
      FormatReader::getCoreMetadataList() const
      {
        assertId(currentId, true);
        return core;
      }

      void
      FormatReader::setMetadataStore(std::shared_ptr<::ome::xml::meta::MetadataStore>& store)
      {
        assertId(currentId, false);

        if (!store)
          throw std::logic_error("MetadataStore can not be null");

        metadataStore = store;
      }

      const std::shared_ptr<::ome::xml::meta::MetadataStore>&
      FormatReader::getMetadataStore() const
      {
        return metadataStore;
      }

      std::shared_ptr<::ome::xml::meta::MetadataStore>&
      FormatReader::getMetadataStore()
      {
        return metadataStore;
      }

      std::vector<std::shared_ptr<::ome::files::FormatReader>>
      FormatReader::getUnderlyingReaders() const
      {
        return std::vector<std::shared_ptr<::ome::files::FormatReader>>();
      }

      bool
      FormatReader::isSingleFile(const std::filesystem::path& /* id */) const
      {
        return true;
      }

      uint32_t
      FormatReader::getRequiredDirectories(const std::vector<std::string>& /* files */) const
      {
        return 0;
      }

      const std::string&
      FormatReader::getDatasetStructureDescription() const
      {
        return datasetDescription;
      }

      bool
      FormatReader::hasCompanionFiles() const
      {
        return companionFiles;
      }

      const std::vector<std::string>&
      FormatReader::getPossibleDomains(const std::string& /* id */) const
      {
        return domains;
      }

      const std::vector<std::string>&
      FormatReader::getDomains() const
      {
        assertId(currentId, true);
        return domains;
      }

      dimension_size_type
      FormatReader::getOptimalTileWidth(dimension_size_type /* channel */) const
      {
        assertId(currentId, true);
        return getSizeX();
      }

      dimension_size_type
      FormatReader::getOptimalTileHeight(dimension_size_type channel) const
      {
        assertId(currentId, true);
        uint32_t bpp = bytesPerPixel(getPixelType());
        dimension_size_type maxHeight = (1024U * 1024U) / (getSizeX() * getRGBChannelCount(channel) * bpp);
        if (!maxHeight)
          maxHeight = 1U;

        return std::min(maxHeight, getSizeY());
      }

      dimension_size_type
      FormatReader::getOptimalTileWidth() const
      {
        assertId(currentId, true);

        dimension_size_type csize = getEffectiveSizeC();
        std::vector<dimension_size_type> widths;
        widths.reserve(csize);
        for (dimension_size_type c = 0; c < csize; ++c)
          widths.push_back(getOptimalTileWidth(c));
        return *std::min_element(widths.begin(), widths.end());
      }

      dimension_size_type
      FormatReader::getOptimalTileHeight() const
      {
        assertId(currentId, true);

        dimension_size_type csize = getEffectiveSizeC();
        std::vector<dimension_size_type> heights;
        heights.reserve(csize);
        for (dimension_size_type c = 0; c < csize; ++c)
          heights.push_back(getOptimalTileHeight(c));
        return *std::min_element(heights.begin(), heights.end());
      }

      dimension_size_type
      FormatReader::getResolutionCount() const
      {
        assertId(currentId, true);

        return core.at(getSeries()).size();
      }

      void
      FormatReader::setResolution(dimension_size_type resolution) const
      {
        if (resolution >= getResolutionCount())
          {
            std::string fs = fmt::format("Invalid resolution: {}", resolution);
            throw std::logic_error(fs);
          }
        // this->series unchanged.
        this->resolution = resolution;
        this->plane = 0;
      }

      dimension_size_type
      FormatReader::getResolution() const
      {
        return resolution;
      }

      void
      FormatReader::setId(const std::filesystem::path& id)
      {
        // Attempt to canonicalize the path.
        path canonicalpath = id;
        try
          {
            canonicalpath = std::filesystem::canonical(id);
          }
        catch (const std::exception&)
          {
          }

        spdlog::debug("{}: Reader initializing {}",
                      readerProperties.name, id.string());

        if (!currentId || canonicalpath != *currentId)
          {
            initFile(canonicalpath);

            const std::shared_ptr<::ome::xml::meta::OMEXMLMetadata>& store =
              std::dynamic_pointer_cast<::ome::xml::meta::OMEXMLMetadata>(getMetadataStore());
            if(store)
              {
                if(saveOriginalMetadata)
                  {
                    MetadataMap allMetadata(metadata);

                    setSeries(0);
                    {
                      SaveSeries(*this);
                      for (dimension_size_type series = 0;
                           series < getSeriesCount();
                           ++series)
                        {
                          std::string name = fmt::format("Series {}", series);

                          try
                            {
                              std::string imageName = store->getImageName(series);
                              if (!imageName.empty())
                                name = imageName;
                            }
                          catch (const std::exception&)
                            {
                            }
                          setSeries(series);
                          const MetadataMap& sm(getSeriesMetadata());
                          for (MetadataMap::const_iterator i = sm.begin();
                               i != sm.end();
                               ++i)
                            allMetadata.set(name + " " + i->first, i->second);
                        }
                    }

                    fillOriginalMetadata(*store, allMetadata);
                  }

                setSeries(0);
                {
                  SaveSeries(*this);

                  for (dimension_size_type series = 0;
                       series < getSeriesCount();
                       ++series)
                    {
                      setSeries(series);

                      if (getModuloZ().size() > 0 || getModuloC().size() > 0 ||
                          getModuloT().size() > 0)
                        {
                          /**
                           * @todo Implement addModuloAlong.  Requires bits of MetadataTools OMEXMLServiceImpl.
                           */
                          // addModuloAlong(store, core.get(series), series);
                        }
                    }
                }
              }
          }
      }

    }
  }
}

/*
 * #%L
 * OME-FILES C++ library for image IO.
 * Copyright © 2006 - 2014 Open Microscopy Environment:
 *   - Massachusetts Institute of Technology
 *   - National Institutes of Health
 *   - University of Dundee
 *   - Board of Regents of the University of Wisconsin-Madison
 *   - Glencoe Software, Inc.
 * Copyright © 2018 Quantitative Imaging Systems, LLC
 * Copyright © 2019 Codelibre Consulting Limited
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of any organization.
 * #L%
 */

#include <cassert>

// Include first due to side effect of MPL vector limit setting which can change the default
// and break multi_index with Boost 1.67
#include <ome/xml/meta/Convert.h>

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_generators.hpp>

#include <fmt/format.h>
#include <spdlog/spdlog.h>

#include <ome/files/FormatException.h>
#include <ome/files/FormatTools.h>
#include <ome/files/MetadataTools.h>
#include <ome/files/out/OMETIFFWriter.h>
#include <ome/files/tiff/Codec.h>
#include <ome/files/tiff/Field.h>
#include <ome/files/tiff/IFD.h>
#include <ome/files/tiff/Tags.h>
#include <ome/files/tiff/TIFF.h>
#include <ome/files/tiff/Util.h>

#include <tiffio.h>

using std::filesystem::path;

using ome::files::getOMEXML;
using ome::files::detail::WriterProperties;
using ome::files::tiff::TIFF;
using ome::files::tiff::IFD;
using ome::files::tiff::enableBigTIFF;

using ome::xml::model::enums::DimensionOrder;
using ome::xml::model::enums::PixelType;
using ome::xml::meta::convert;
using ome::xml::meta::MetadataRetrieve;
using ome::xml::meta::OMEXMLMetadata;

namespace ome
{
  namespace files
  {
    namespace out
    {

      namespace
      {

        WriterProperties
        tiff_properties()
        {
          WriterProperties p("OME-TIFF",
                             "Open Microscopy Environment TIFF");

          // Note that tf2, tf8 and btf are all extensions for
          // "bigTIFF" (2nd generation TIFF, TIFF with 8-byte offsets
          // and big TIFF respectively).
          p.suffixes = {"ome.tif", "ome.tiff", "ome.tf2", "ome.tf8", "ome.btf"};

          for (const auto& pixeltype : PixelType::values())
            {
              const std::vector<std::string>& ptcodecs = tiff::getCodecNames(pixeltype.first);
              std::set<std::string> codecset(ptcodecs.begin(), ptcodecs.end());
              // Supported by default with no compression
              codecset.insert("default");
              p.compression_types.insert(codecset.begin(), codecset.end());
              p.pixel_compression_types.insert(WriterProperties::pixel_compression_type_map::value_type(pixeltype.first, codecset));
            }

          return p;
        }

        const WriterProperties props(tiff_properties());

        const std::vector<path> companion_suffixes{"companion.ome"};

        const std::string default_description("OME-TIFF");

      }

      OMETIFFWriter::TIFFState::TIFFState(std::shared_ptr<ome::files::tiff::TIFF>& tiff):
        uuid(boost::uuids::to_string(boost::uuids::random_generator()())),
        tiff(tiff),
        ifdCount(0U)
      {
      }

      OMETIFFWriter::TIFFState::~TIFFState()
      {
      }

      OMETIFFWriter::OMETIFFWriter():
        ome::files::detail::FormatWriter(props),
        files(),
        tiffs(),
        currentTIFF(tiffs.end()),
        flags(),
        seriesState(),
        originalMetadataRetrieve(),
        omeMeta(),
        bigTIFF(std::nullopt)
      {
      }

      OMETIFFWriter::~OMETIFFWriter()
      {
        try
          {
            close();
          }
        catch (...)
          {
          }
      }

      void
      OMETIFFWriter::setId(const std::filesystem::path& id)
      {
        // Attempt to canonicalize the path.
        path canonicalpath = id;
        try
          {
            canonicalpath = std::filesystem::canonical(id);
          }
        catch (const std::exception&)
          {
          }

        if (currentId && *currentId == canonicalpath)
          return;

        if (seriesState.empty()) // First call to setId.
          {
            baseDir = (canonicalpath.parent_path());

            // Create OME-XML metadata.
            originalMetadataRetrieve = metadataRetrieve;
            omeMeta = std::make_shared<OMEXMLMetadata>();
            convert(*metadataRetrieve, *omeMeta);
            omeMeta->resolveReferences();
            metadataRetrieve = omeMeta;

            // Try to fix up OME-XML metadata if inconsistent.
            if (!validateModel(*omeMeta, false))
              {
                validateModel(*omeMeta, true);
                if (validateModel(*omeMeta, false))
                  {
                    spdlog::warn("Correction of model SizeC/ChannelCount/SamplesPerPixel inconsistency attempted");
                  }
                else
                  {
                    spdlog::error("Correction of model SizeC/ChannelCount/SamplesPerPixel inconsistency attempted (but inconsistencies remain)");
                  }
              }

            // Set up initial TIFF plane state for all planes in each series.
            dimension_size_type seriesCount = metadataRetrieve->getImageCount();
            seriesState.resize(seriesCount);
            for (dimension_size_type series = 0U; series < seriesCount; ++series)
              {
                dimension_size_type sizeZ = metadataRetrieve->getPixelsSizeZ(series);
                dimension_size_type sizeT = metadataRetrieve->getPixelsSizeT(series);
                dimension_size_type effC = metadataRetrieve->getChannelCount(series);
                dimension_size_type planeCount = sizeZ * sizeT * effC;

                SeriesState& seriesMeta(seriesState.at(series));
                seriesMeta.planes.resize(planeCount);

                for (dimension_size_type plane = 0U; plane < planeCount; ++plane)
                  {
                    detail::OMETIFFPlane& planeMeta(seriesMeta.planes.at(plane));
                    planeMeta.certain = true;
                    planeMeta.status = detail::OMETIFFPlane::ABSENT; // Not written yet.
                  }
              }
          }

        if (flags.empty())
          {
            flags += 'w';

            // Get expected size of pixel data.
            std::shared_ptr<const ::ome::xml::meta::MetadataRetrieve> mr(getMetadataRetrieve());
            storage_size_type pixelSize = significantPixelSize(*mr);

            if (enableBigTIFF(bigTIFF, pixelSize, canonicalpath))
              flags += '8';
          }

        tiff_map::iterator i = tiffs.find(canonicalpath);
        if (i == tiffs.end())
          {
            detail::FormatWriter::setId(canonicalpath);
            std::shared_ptr<ome::files::tiff::TIFF> tiff(ome::files::tiff::TIFF::open(canonicalpath, flags));
            std::pair<tiff_map::iterator,bool> result =
              tiffs.insert(tiff_map::value_type(*currentId, TIFFState(tiff)));
            if (result.second) // should always be true
              currentTIFF = result.first;
            detail::FormatWriter::setId(id);
            setupIFD();
          }
        else
          {
            detail::FormatWriter::setId(i->first);
            currentTIFF = i;
          }
      }

      void
      OMETIFFWriter::close(bool fileOnly)
      {
        try
          {
            if (currentId)
              {
                // Flush last IFD if unwritten.
                if(currentTIFF != tiffs.end())
                  {
                    nextIFD();
                    currentTIFF = tiffs.end();
                  }

                // Remove any BinData and old TiffData elements.
                removeBinData(*omeMeta);
                removeTiffData(*omeMeta);
                // Create UUID and TiffData elements for each series.
                fillMetadata();

                for (auto& tiff : tiffs)
                  {
                    // Get OME-XML for this TIFF file.
                    std::string xml = getOMEXML(tiff.first);
                    // Make sure file is closed before we modify it outside libtiff.
                    tiff.second.tiff->close();

                    // Save OME-XML in the TIFF.
                    saveComment(tiff.first, xml);
                  }
              }

            // Close any open TIFFs.
            for (auto& tiff : tiffs)
              tiff.second.tiff->close();

            files.clear();
            tiffs.clear();
            currentTIFF = tiffs.end();
            flags.clear();
            seriesState.clear();
            originalMetadataRetrieve.reset();
            omeMeta.reset();
            bigTIFF = std::nullopt;

            ome::files::detail::FormatWriter::close(fileOnly);
          }
        catch (const std::exception&)
          {
            currentTIFF = tiffs.end(); // Ensure we only flush the last IFD once.
            ome::files::detail::FormatWriter::close(fileOnly);
            throw;
          }
      }

      void
      OMETIFFWriter::setSeries(dimension_size_type series)
      {
        const dimension_size_type currentSeries = getSeries();
        detail::FormatWriter::setSeries(series);

        if (currentSeries != series)
          {
            nextIFD();
            setupIFD();
          }
      }

      void
      OMETIFFWriter::setResolution(dimension_size_type resolution)
      {
        const dimension_size_type currentResolution = getResolution();
        detail::FormatWriter::setResolution(resolution);

        if (currentResolution != resolution)
          {
            nextSUBIFD();
            setupIFD();
          }
      }

      void
      OMETIFFWriter::setPlane(dimension_size_type plane)
      {
        const dimension_size_type currentPlane = getPlane();
        detail::FormatWriter::setPlane(plane);

        if (currentPlane != plane)
          {
            nextIFD();
            setupIFD();
          }
      }

      dimension_size_type
      OMETIFFWriter::getTileSizeX() const
      {
        // Get current IFD.  Also requires unset size (fallback) or
        // nonzero set size.
        if (currentId && (!this->tile_size_x ||
                          (this->tile_size_x && *this->tile_size_x)))
          {
            std::shared_ptr<tiff::IFD> ifd (currentTIFF->second.tiff->getCurrentDirectory());
            return ifd->getTileWidth();
          }
        else // setId not called yet; fall back.
          return detail::FormatWriter::getTileSizeX();
      }

      dimension_size_type
      OMETIFFWriter::getTileSizeY() const
      {
        // Get current IFD.  Also requires unset size (fallback) or
        // nonzero set size.
        if (currentId && (!this->tile_size_y ||
                          (this->tile_size_y && *this->tile_size_y)))
          {
            std::shared_ptr<tiff::IFD> ifd (currentTIFF->second.tiff->getCurrentDirectory());
            return ifd->getTileWidth();
          }
        else // setId not called yet; fall back.
          return detail::FormatWriter::getTileSizeY();
      }

      void
      OMETIFFWriter::nextIFD()
      {
        currentTIFF->second.tiff->writeCurrentDirectory();
        ++currentTIFF->second.ifdCount;
      }

      void
      OMETIFFWriter::nextSUBIFD()
      {
        currentTIFF->second.tiff->writeCurrentDirectory();
      }

      void
      OMETIFFWriter::setupIFD()
      {
        // Get current IFD.
        std::shared_ptr<tiff::IFD> ifd (currentTIFF->second.tiff->getCurrentDirectory());

        ifd->setImageWidth(getSizeX());
        ifd->setImageHeight(getSizeY());

        // Default strip or tile size.  We base this upon a default
        // chunk size of 64KiB for greyscale images, which will
        // increase to 192KiB for 3 sample RGB images.  We use strips
        // up to a width of 2048 after which tiles are used.
        if(getSizeX() == 0)
          {
            throw FormatException("Can't set strip or tile size: SizeX is 0");
          }
        else if(!this->tile_size_x && this->tile_size_y)
          {
            // Manually set strip size if the size is positive.  Or
            // else set strips of size 1 as a fallback for
            // compatibility with Bio-Formats.
            if(*this->tile_size_y)
              {
                ifd->setTileType(tiff::STRIP);
                ifd->setTileWidth(getSizeX());
                ifd->setTileHeight(*this->tile_size_y);
              }
            else
              {
                ifd->setTileType(tiff::STRIP);
                ifd->setTileWidth(getSizeX());
                ifd->setTileHeight(1U);
              }
          }
        else if(this->tile_size_x && this->tile_size_y)
          {
            // Manually set tile size if both sizes are positive.  Or
            // else set strips of size 1 as a fallback for
            // compatibility with Bio-Formats.
            if(*this->tile_size_x && *this->tile_size_y)
              {
                ifd->setTileType(tiff::TILE);
                ifd->setTileWidth(*this->tile_size_x);
                ifd->setTileHeight(*this->tile_size_y);
              }
            else
              {
                ifd->setTileType(tiff::STRIP);
                ifd->setTileWidth(getSizeX());
                ifd->setTileHeight(1U);
              }
          }
        else if(getSizeX() < 2048)
          {
            // Default to strips, mainly for compatibility with
            // readers which don't support tiles.
            ifd->setTileType(tiff::STRIP);
            ifd->setTileWidth(getSizeX());
            uint32_t height = 65536U / getSizeX();
            if (height == 0)
              height = 1;
            ifd->setTileHeight(height);
          }
        else
          {
            // Default to tiles.
            ifd->setTileType(tiff::TILE);
            ifd->setTileWidth(256U);
            ifd->setTileHeight(256U);
          }

        std::array<dimension_size_type, 3> coords = getZCTCoords(getPlane());

        dimension_size_type channel = coords[1];

        ifd->setPixelType(getPixelType());
        ifd->setBitsPerSample(bitsPerPixel(getPixelType()));
        ifd->setSamplesPerPixel(getRGBChannelCount(channel));

        const std::optional<bool> interleaved(getInterleaved());
        if (interleaved && *interleaved)
          ifd->setPlanarConfiguration(tiff::CONTIG);
        else
          ifd->setPlanarConfiguration(tiff::SEPARATE);

        // This isn't necessarily always true; we might want to use a
        // photometric interpretation other than RGB with three
        // samples.
        if (isRGB(channel) && getRGBChannelCount(channel) == 3)
          ifd->setPhotometricInterpretation(tiff::RGB);
        else
          ifd->setPhotometricInterpretation(tiff::MIN_IS_BLACK);

        const std::optional<std::string> compression(getCompression());
        if(compression)
          ifd->setCompression(tiff::getCodecScheme(*compression));

        if (currentTIFF->second.ifdCount == 0)
          ifd->getField(ome::files::tiff::IMAGEDESCRIPTION).set(default_description);

        // Set up SubIFD if this is a full-resolution image and
        // sub-resolution images are present.
        if (getResolution() == 0)
          {
            ifd->getField(ome::files::tiff::SUBFILETYPE).set(ome::files::tiff::SUBFILETYPE_PAGE);
            if (getResolutionCount() > 1)
              {
                ifd->setSubIFDCount(getResolutionCount() - 1);
              }
          }
        else
          {
            ifd->getField(ome::files::tiff::SUBFILETYPE).set
              (ome::files::tiff::SUBFILETYPE_PAGE|ome::files::tiff::SUBFILETYPE_REDUCEDIMAGE);
          }

        currentIFD = currentTIFF->second.tiff->getCurrentDirectory();
      }

      void
      OMETIFFWriter::saveBytes(dimension_size_type plane,
                               VariantPixelBuffer& buf,
                               dimension_size_type x,
                               dimension_size_type y,
                               dimension_size_type w,
                               dimension_size_type h)
      {
        assertId(currentId, true);

        setPlane(plane);

        // Get plane metadata.
        detail::OMETIFFPlane& planeMeta(seriesState.at(getSeries()).planes.at(plane));

        currentIFD->writeImage(buf, x, y, w, h);

        // Set plane metadata.
        if (getResolution() == 0)
          {
            planeMeta.id = currentTIFF->first;
            planeMeta.index = currentTIFF->second.ifdCount;
            planeMeta.ifd = nullptr; // Unused for writing.
            planeMeta.certain = true;
            planeMeta.status = detail::OMETIFFPlane::PRESENT; // Plane now written.
          }
      }

      void
      OMETIFFWriter::fillMetadata()
      {
        if (!omeMeta)
          throw std::logic_error("OMEXMLMetadata null");

        dimension_size_type badPlanes = 0U;
        for (const auto& series : seriesState)
          for (const auto& plane : series.planes)
            if (plane.status != detail::OMETIFFPlane::PRESENT) // Plane not written.
              ++badPlanes;

        if (badPlanes)
          {
            std::string fs = fmt::format
              ("Inconsistent writer state: {} planes have not been written",
               badPlanes);
            throw FormatException(fs);
          }

        dimension_size_type seriesCount = getSeriesCount();

        for (dimension_size_type series = 0U; series < seriesCount; ++series)
          {
            DimensionOrder dimOrder = metadataRetrieve->getPixelsDimensionOrder(series);
            dimension_size_type sizeZ = metadataRetrieve->getPixelsSizeZ(series);
            dimension_size_type sizeT = metadataRetrieve->getPixelsSizeT(series);
            dimension_size_type effC = metadataRetrieve->getChannelCount(series);
            dimension_size_type imageCount = sizeZ * sizeT * effC;

            if (imageCount == 0)
              {
                omeMeta->setTiffDataPlaneCount(0, series, 0);
              }

            for (dimension_size_type plane = 0U; plane < imageCount; ++plane)
              {
                std::array<dimension_size_type, 3> coords =
                  ome::files::getZCTCoords(dimOrder, sizeZ, effC, sizeT, imageCount, plane);
                const detail::OMETIFFPlane& planeState(seriesState.at(series).planes.at(plane));

                tiff_map::const_iterator t = tiffs.find(planeState.id);
                if (t != tiffs.end())
                  {
                    path relative(std::filesystem::relative(planeState.id, baseDir));
                    std::string uuid("urn:uuid:");
                    uuid += t->second.uuid;
                    omeMeta->setUUIDFileName(relative.string(), series, plane);
                    omeMeta->setUUIDValue(uuid, series, plane);

                    // Fill in non-default TiffData attributes.
                    omeMeta->setTiffDataFirstZ(coords[0], series, plane);
                    omeMeta->setTiffDataFirstT(coords[2], series, plane);
                    omeMeta->setTiffDataFirstC(coords[1], series, plane);
                    omeMeta->setTiffDataIFD(planeState.index, series, plane);
                    omeMeta->setTiffDataPlaneCount(1, series, plane);
                  }
                else
                  {
                    std::string fs = fmt::format
                      ("Inconsistent writer state: TIFF file {} not registered with a UUID",
                       planeState.id.string());
                    throw FormatException(fs);
                  }
              }
          }
      }

      std::string
      OMETIFFWriter::getOMEXML(const std::filesystem::path& id)
      {
        tiff_map::const_iterator t = tiffs.find(id);

        if (t == tiffs.end())
          {
            std::string fs = fmt::format
              ("Inconsistent writer state: TIFF file {} not registered with a UUID", id.string());
            throw FormatException(fs);
          }

        path relative(std::filesystem::relative(id, baseDir));
        std::string uuid("urn:uuid:");
        uuid += t->second.uuid;
        omeMeta->setUUID(uuid);

        bool validate = true;
#ifdef OME_HAVE_QT5_XSLT
        validate = false;
#endif
#ifdef OME_HAVE_QT6_DOM
        validate = false;
#endif

        return files::getOMEXML(*omeMeta, validate);
      }

      void
      OMETIFFWriter::saveComment(const std::filesystem::path& id,
                                 const std::string&           xml)
      {
        // Rewrite first IFD.
        auto tiff = ome::files::tiff::TIFF::open(id, "r+");
        auto ifd = tiff->getDirectoryByIndex(0U);
        ifd->getField(ome::files::tiff::IMAGEDESCRIPTION).set(xml);
        tiff->writeDirectory(ifd);
      }

      void
      OMETIFFWriter::setBigTIFF(std::optional<bool> big)
      {
        bigTIFF = big;
      }

      std::optional<bool>
      OMETIFFWriter::getBigTIFF() const
      {
        return bigTIFF;
      }

    }
  }
}

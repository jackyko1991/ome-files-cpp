/*
 * #%L
 * OME-FILES C++ library for image IO.
 * %%
 * Copyright © 2014 - 2015 Open Microscopy Environment:
 *   - Massachusetts Institute of Technology
 *   - National Institutes of Health
 *   - University of Dundee
 *   - Board of Regents of the University of Wisconsin-Madison
 *   - Glencoe Software, Inc.
 * Copyright © 2018 Quantitative Imaging Systems, LLC
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of any organization.
 * #L%
 */

#include <filesystem>
#include <regex>
#include <iostream>

#include <ome/files/tiff/Types.h>

#include "tiffsamples.h"

using namespace std::filesystem;

std::vector<TIFFTestParameters>
find_tiff_tests()
{
  std::vector<TIFFTestParameters> params;

  path dir(PROJECT_BINARY_DIR "/test/data");
  if (exists(dir) && is_directory(dir))
    {
      for(directory_iterator i(dir); i != directory_iterator(); ++i)
        {
          static std::regex tile_match(".*/data-layout-([[:digit:]]+)x([[:digit:]]+)-([[:alpha:]]+)-tiles-([[:digit:]]+)x([[:digit:]]+)\\.tiff");
          static std::regex strip_match(".*/data-layout-([[:digit:]]+)x([[:digit:]]+)-([[:alpha:]]+)-strips-([[:digit:]]+)\\.tiff");
          static std::regex plain_match(".*/data-layout-([[:digit:]]+)x([[:digit:]]+)-([[:alpha:]]+)\\.tiff");

          std::smatch found;
          std::string file(i->path().generic_string());
          path wpath(i->path().parent_path());
          wpath /= std::string("w-") + i->path().filename().generic_string();
          std::string wfile(wpath.generic_string());
          if (std::regex_match(file, found, tile_match))
            {
              TIFFTestParameters p;
              p.tile = ome::files::tiff::TILE;
              p.file = file;
              p.wfile = wfile;
              p.compression = ome::files::tiff::COMPRESSION_NONE;

              std::istringstream iwid(found[1]);
              if (!(iwid >> p.imagewidth))
                continue;

              std::istringstream iht(found[2]);
              if (!(iht >> p.imagelength))
                continue;

              p.imageplanar = false;
              if (found[3] == "planar")
                p.imageplanar = true;

              p.tilewidth = 0;
              std::istringstream twid(found[4]);
              if (!(twid >> *p.tilewidth))
                continue;

              p.tilelength = 0;
              std::istringstream tht(found[5]);
              if (!(tht >> *p.tilelength))
                continue;

              params.push_back(p);
            }
          else if (std::regex_match(file, found, strip_match))
            {
              TIFFTestParameters p;
              p.tile = ome::files::tiff::STRIP;
              p.file = file;
              p.wfile = wfile;
              p.compression = ome::files::tiff::COMPRESSION_NONE;

              std::istringstream iwid(found[1]);
              if (!(iwid >> p.imagewidth))
                continue;

              std::istringstream iht(found[2]);
              if (!(iht >> p.imagelength))
                continue;

              p.imageplanar = false;
              if (found[3] == "planar")
                p.imageplanar = true;

              p.tilewidth = std::nullopt;

              p.tilelength = 0;
              std::istringstream srow(found[4]);
              if (!(srow >> *p.tilelength))
                continue;

              params.push_back(p);
            }
          else if (std::regex_match(file, found, plain_match))
            {
              TIFFTestParameters p;
              p.tile = std::nullopt;
              p.file = file;
              p.wfile = wfile;
              p.compression = ome::files::tiff::COMPRESSION_NONE;

              std::istringstream iwid(found[1]);
              if (!(iwid >> p.imagewidth))
                continue;

              std::istringstream iht(found[2]);
              if (!(iht >> p.imagelength))
                continue;

              p.imageplanar = false;
              if (found[3] == "planar")
                p.imageplanar = true;

              p.tilewidth = std::nullopt;
              p.tilelength = std::nullopt;

              params.push_back(p);
            }
        }
    }
    else
    {
        std::cerr << "Test files not found" << std::endl;
    }

    if (params.empty())
    {
        std:: cout << "No test files found" << std::endl;
    }

  std::cout << "Found " << params.size() << " TIFF tests\n";

  return params;
}
